package com.mycompany.modulpraktikum3c;

import javax.swing.*;
import java.awt.Container;

public class Tugas2 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 300;

    public static void main(String[] args) {
        Tugas2 dialog = new Tugas2();
        dialog.setVisible(true);
    }

    public Tugas2() {
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("Button Test");
        Container contentPane;
        JButton YELLOW, BLUE, RED;
        setLayout(null);
        contentPane = getContentPane();

        YELLOW = new JButton("YELLOW");
        YELLOW.setBounds(0, 0, 100, 50);
        BLUE = new JButton("BLUE");
        BLUE.setBounds(100, 0, 100, 50);
        RED = new JButton("RED");
        RED.setBounds(200, 0, 100, 50);
        contentPane.add(YELLOW);
        contentPane.add(BLUE);
        contentPane.add(RED);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
