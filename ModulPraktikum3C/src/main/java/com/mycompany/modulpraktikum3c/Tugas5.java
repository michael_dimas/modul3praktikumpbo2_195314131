package com.mycompany.modulpraktikum3c;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.*;

public class Tugas5 extends JDialog{
    
    public Tugas5(){
    this.setBounds(100,100,500,200);
    this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    this.setTitle("RadioButtonDemo");
    this.setLayout(new BorderLayout());
    JPanel panelButton = new JPanel();
    JButton left = new JButton("Left");
    JButton right = new JButton("Right");
    panelButton.add(left);
    panelButton.add(right);
    JPanel panelText = new JPanel();
    JTextArea textArea = new JTextArea(5,27);
    textArea.setText("WelcomeToJava");
    panelText.add(textArea);
    JPanel panelCheckBox = new JPanel(new GridLayout(3,1));
    JCheckBox centered = new JCheckBox("Centered");
    JCheckBox bold = new JCheckBox("Bold");
    JCheckBox italic = new JCheckBox("Italic");
    panelCheckBox.add(centered);
    panelCheckBox.add(bold);
    panelCheckBox.add(italic);
    JPanel panelJRadio = new JPanel(new GridLayout(3,1));
    JRadioButton red = new JRadioButton("Red");
    JRadioButton green = new JRadioButton("Green");
    JRadioButton blue = new JRadioButton("Blue");
    panelJRadio.add(red);
    panelJRadio.add(green);
    panelJRadio.add(blue);
    this.add(panelButton, BorderLayout.SOUTH);
    this.add(panelButton, BorderLayout.CENTER);
    this.add(panelButton, BorderLayout.EAST);
    this.add(panelButton, BorderLayout.WEST);
    this.setVisible(true);
    }
    
    public static void main(String[] args) {
        new Tugas5();
    }
}
