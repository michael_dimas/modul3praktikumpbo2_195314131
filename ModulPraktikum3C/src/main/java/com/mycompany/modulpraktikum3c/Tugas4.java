package com.mycompany.modulpraktikum3c;
import java.awt.*;
import javax.swing.*;
public class Tugas4 extends JDialog{
    
    public Tugas4(){
        JFrame frame = new JFrame();
        this.setPreferredSize(new Dimension(400,500));
        this.pack();
        this.setTitle("Chech Box Demo");
        this.setLayout(new BorderLayout());
        this.setLocationRelativeTo(frame);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        JPanel textPanel = new JPanel();
        JTextField text1 = new JTextField("\tWelcome to Java");
        text1.setEditable(false);
        text1.setBounds(1,1,200,150);
        textPanel.add(text1);
        JPanel checkboxPanel = new JPanel();
        JCheckBox checkBox1 = new JCheckBox();
        checkBox1.setText("Centered");
        checkBox1.setBounds(100,100,50,50);
        JCheckBox checkBox2 = new JCheckBox();
        checkBox2.setText("Bold");
        checkBox2.setBounds(200,120,50,50);
        JCheckBox checkBox3 = new JCheckBox();
        checkBox3.setText("Italic");
        checkBox3.setBounds(300,140,50,50);
        checkboxPanel.add(checkBox1);
        checkboxPanel.add(checkBox2);
        checkboxPanel.add(checkBox3);
        JPanel buttonPanel = new JPanel();
        JButton button1 = new JButton();
        JButton b1 = new JButton();
        b1.setText("Left");
        JButton b2 = new JButton();
        b2.setText("Right");
        buttonPanel.add(b1);
        buttonPanel.add(b2);
        this.add(textPanel, BorderLayout.WEST);
        this.add(checkboxPanel, BorderLayout.EAST);
        this.add(buttonPanel, BorderLayout.SOUTH);
        this.setVisible(true);
    }
    
    public static void main(String[] args) {
        new Tugas4();
    }
}
