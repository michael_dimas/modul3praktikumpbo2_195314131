package com.mycompany.modulpraktikum3c;

import javax.swing.*;

public class Tugas1 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 300;
    
    private JMenuBar menuUtama;
    private JMenu menu1;
    private JMenu menu2;
    private JMenuItem item1;
    private JMenuItem item2;
    
    public static void main(String[] args) {
        Tugas1 frame = new Tugas1();
        frame.setVisible(true);
    }
    
    public Tugas1() {
        this.setTitle("Frame Pertama");
        menuUtama = new JMenuBar();
        menu1 = new JMenu("File");
        menu2 = new JMenu("Edit");
        item1 = new JMenuItem("Tampil 1");
        item2 = new JMenuItem("Tampil 2");
        menuUtama.add(menu1);
        menuUtama.add(menu2);
        menu1.add(item1);
        menu1.add(item2);
        this.setJMenuBar(menuUtama);
        
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
