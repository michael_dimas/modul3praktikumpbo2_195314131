package com.mycompany.modulpraktikum3c;

import java.net.URL;
import javax.swing.*;

public class Tugas3 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 300;
    private JLabel labelGambar;
    private JLabel labelNamaGambar;

    public static void main(String[] args) {
        Tugas3 dialog = new Tugas3();
        dialog.setVisible(true);
        dialog.setTitle("Text and Icon Label");
    }

    public Tugas3() {
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        GambarLabel();
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }

    public void GambarLabel() {
        URL grape = this.getClass().getResource("grape.jpg");
        ImageIcon imageGrape = new ImageIcon(grape);
        labelGambar = new JLabel(imageGrape);
        labelGambar.setBounds(100, 110, 250, 200);
        this.add(labelGambar);
        labelNamaGambar = new JLabel("Grapes");
        labelNamaGambar.setBounds(200, 330, 80, 30);
        this.add(labelNamaGambar);
    }
}
