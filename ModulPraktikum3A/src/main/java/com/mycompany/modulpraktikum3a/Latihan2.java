package com.mycompany.modulpraktikum3a;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
public class Latihan2 extends JFrame{
   
        public Latihan2(){
            this.setSize(300,500);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setTitle("Ini Class turunan dari class JFrame");
            this.setVisible(true);
            
            JPanel panel = new JPanel();
            JButton tombol = new JButton();
            tombol.setText("Ini Tombol");
            panel.add(tombol);
            this.add(panel);
        }
    public static void main(String[] args) {
        new Latihan2();
    }
}
