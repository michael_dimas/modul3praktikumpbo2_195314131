package com.mycompany.modulpraktikum3a;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

public class Latihan3 extends JFrame {

    public Latihan3() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        this.add(panel);

        JLabel label = new JLabel("Labelku");
        panel.add(label);
        this.add(panel);

        JTextField field = new JTextField("TextField");
        panel.add(field);
        this.add(panel);

        JCheckBox box = new JCheckBox("Check Box");
        panel.add(box);
        this.add(panel);

        JRadioButton radio = new JRadioButton("Radio Button");
        panel.add(radio);
        this.add(panel);
    }

    public static void main(String[] args) {
        new Latihan3();
    }
}
