package com.mycompany.modulpraktikum3a;
import javax.swing.*;
public class Latihan4 extends JFrame{
    public Latihan4(){
        this.setLayout(null);
        this.setSize(300,100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);

        //JPanel panel =new JPanel();

        JLabel label= new JLabel("Keyboard");
        label.setBounds(150,20,100,20);
        this.add(label);


        JTextField text = new JTextField();
        text.setBounds(150,50,150,20);
        this.add(text);
        
        JButton tombol = new JButton("Find");
        tombol.setBounds(150, 70, 100, 20);
        this.add(tombol);
    }
    public static void main(String[] args) {
        new Latihan4();
    }
}
